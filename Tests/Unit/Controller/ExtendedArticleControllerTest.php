<?php
namespace HNY\HnyExtExtendedArticle\Tests\Unit\Controller;

/**
 * Test case.
 */
class ExtendedArticleControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyExtExtendedArticle\Controller\ExtendedArticleController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HNY\HnyExtExtendedArticle\Controller\ExtendedArticleController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllExtendedArticlesFromRepositoryAndAssignsThemToView()
    {

        $allExtendedArticles = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $extendedArticleRepository = $this->getMockBuilder(\HNY\HnyExtExtendedArticle\Domain\Repository\ExtendedArticleRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $extendedArticleRepository->expects(self::once())->method('findAll')->will(self::returnValue($allExtendedArticles));
        $this->inject($this->subject, 'extendedArticleRepository', $extendedArticleRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('extendedArticles', $allExtendedArticles);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenExtendedArticleToView()
    {
        $extendedArticle = new \HNY\HnyExtExtendedArticle\Domain\Model\ExtendedArticle();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('extendedArticle', $extendedArticle);

        $this->subject->showAction($extendedArticle);
    }
}
