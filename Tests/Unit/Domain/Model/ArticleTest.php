<?php
namespace HNY\HnyExtExtendedArticle\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ArticleTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyExtExtendedArticle\Domain\Model\Article
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HNY\HnyExtExtendedArticle\Domain\Model\Article();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getString1ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getString1()
        );
    }

    /**
     * @test
     */
    public function setString1ForStringSetsString1()
    {
        $this->subject->setString1('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'string1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getString2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getString2()
        );
    }

    /**
     * @test
     */
    public function setString2ForStringSetsString2()
    {
        $this->subject->setString2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'string2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getString3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getString3()
        );
    }

    /**
     * @test
     */
    public function setString3ForStringSetsString3()
    {
        $this->subject->setString3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'string3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getString4ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getString4()
        );
    }

    /**
     * @test
     */
    public function setString4ForStringSetsString4()
    {
        $this->subject->setString4('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'string4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText1ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText1()
        );
    }

    /**
     * @test
     */
    public function setText1ForStringSetsText1()
    {
        $this->subject->setText1('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText2()
        );
    }

    /**
     * @test
     */
    public function setText2ForStringSetsText2()
    {
        $this->subject->setText2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText3()
        );
    }

    /**
     * @test
     */
    public function setText3ForStringSetsText3()
    {
        $this->subject->setText3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime1ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime1()
        );
    }

    /**
     * @test
     */
    public function setDateTime1ForDateTimeSetsDateTime1()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime1($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime2ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime2()
        );
    }

    /**
     * @test
     */
    public function setDateTime2ForDateTimeSetsDateTime2()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime2($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime3ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime3()
        );
    }

    /**
     * @test
     */
    public function setDateTime3ForDateTimeSetsDateTime3()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime3($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime4ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDateTime4()
        );
    }

    /**
     * @test
     */
    public function setDateTime4ForStringSetsDateTime4()
    {
        $this->subject->setDateTime4('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dateTime4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneImage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneImage->attach($image);
        $this->subject->setImage($objectStorageHoldingExactlyOneImage);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneImage,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addImageToObjectStorageHoldingImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $imageObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $imageObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($image));
        $this->inject($this->subject, 'image', $imageObjectStorageMock);

        $this->subject->addImage($image);
    }

    /**
     * @test
     */
    public function removeImageFromObjectStorageHoldingImage()
    {
        $image = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $imageObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $imageObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($image));
        $this->inject($this->subject, 'image', $imageObjectStorageMock);

        $this->subject->removeImage($image);
    }

    /**
     * @test
     */
    public function getFileReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFile()
        );
    }

    /**
     * @test
     */
    public function setFileForFileReferenceSetsFile()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneFile = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFile->attach($file);
        $this->subject->setFile($objectStorageHoldingExactlyOneFile);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneFile,
            'file',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFileToObjectStorageHoldingFile()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $fileObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fileObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($file));
        $this->inject($this->subject, 'file', $fileObjectStorageMock);

        $this->subject->addFile($file);
    }

    /**
     * @test
     */
    public function removeFileFromObjectStorageHoldingFile()
    {
        $file = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $fileObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fileObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($file));
        $this->inject($this->subject, 'file', $fileObjectStorageMock);

        $this->subject->removeFile($file);
    }
}
