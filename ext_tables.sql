#
# Table structure for table 'tx_hnyextextendedarticle_domain_model_article'
#
CREATE TABLE tx_hnyextextendedarticle_domain_model_article (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	subtitle varchar(255) DEFAULT '' NOT NULL,
	string1 varchar(255) DEFAULT '' NOT NULL,
	string2 varchar(255) DEFAULT '' NOT NULL,
	string3 varchar(255) DEFAULT '' NOT NULL,
	string4 varchar(255) DEFAULT '' NOT NULL,
	text1 text,
	text2 text,
	text3 text,
	date_time1 int(11) DEFAULT '0' NOT NULL,
	date_time2 int(11) DEFAULT '0' NOT NULL,
	date_time3 int(11) DEFAULT '0' NOT NULL,
	date_time4 varchar(255) DEFAULT '' NOT NULL,
	image int(11) unsigned DEFAULT '0' NOT NULL,
	file int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted smallint(5) unsigned DEFAULT '0' NOT NULL,
	hidden smallint(5) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state smallint(6) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_hnyextextendedarticle_domain_model_article'
#
CREATE TABLE tx_hnyextextendedarticle_domain_model_article (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder