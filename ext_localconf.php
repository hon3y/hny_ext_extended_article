<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticlelist',
            [
                'Article' => 'list'
            ],
            // non-cacheable actions
            [
                'Article' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleshow',
            [
                'Article' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Article' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleloadmore',
            [
                'Article' => 'loadMore'
            ],
            // non-cacheable actions
            [
                'Article' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleloadlist',
            [
                'Article' => 'loadList'
            ],
            // non-cacheable actions
            [
                'Article' => 'loadList'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hnyextextendedarticlelist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hny_ext_extended_article') . 'Resources/Public/Icons/user_plugin_hnyextextendedarticlelist.svg
                        title = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticlelist
                        description = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticlelist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hnyextextendedarticle_hnyextextendedarticlelist
                        }
                    }
                    hnyextextendedarticleshow {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hny_ext_extended_article') . 'Resources/Public/Icons/user_plugin_hnyextextendedarticleshow.svg
                        title = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleshow
                        description = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleshow.description
                        tt_content_defValues {
                            CType = list
                            list_type = hnyextextendedarticle_hnyextextendedarticleshow
                        }
                    }
                    hnyextextendedarticleloadmore {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hny_ext_extended_article') . 'Resources/Public/Icons/user_plugin_hnyextextendedarticleloadmore.svg
                        title = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleloadmore
                        description = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleloadmore.description
                        tt_content_defValues {
                            CType = list
                            list_type = hnyextextendedarticle_hnyextextendedarticleloadmore
                        }
                    }
                    hnyextextendedarticleloadlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hny_ext_extended_article') . 'Resources/Public/Icons/user_plugin_hnyextextendedarticleloadlist.svg
                        title = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleloadlist
                        description = LLL:EXT:hny_ext_extended_article/Resources/Private/Language/locallang_db.xlf:tx_hny_ext_extended_article_domain_model_hnyextextendedarticleloadlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hnyextextendedarticle_hnyextextendedarticleloadlist
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {
        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hnyextextendedarticlelist >
                wizards.newContentElement.wizardItems.plugins.elements.hnyextextendedarticleshow >
                wizards.newContentElement.wizardItems.plugins.elements.hnyextextendedarticleloadmore >
                wizards.newContentElement.wizardItems.plugins.elements.hnyextextendedarticleloadlist >
                wizards.newContentElement.wizardItems.hon3y {
                    header = Hon3y
                    after = common,special,menu,plugins,forms
                    elements {
                        hnyextextendedarticlelist {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Extended Article :: List
                            description = List
                            tt_content_defValues {
                                 CType = list
                                 list_type = hnyextextendedarticle_hnyextextendedarticlelist
                            }
                        }
                        hnyextextendedarticleshow {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Extended Article :: Show
                            description = Detail
                            tt_content_defValues {
                                 CType = list
                                 list_type = hnyextextendedarticle_hnyextextendedarticleshow
                            }
                        }
                        hnyextextendedarticleloadmore {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Extended Article :: Load more
                            description = Ajax loader
                            tt_content_defValues {
                                 CType = list
                                 list_type = hnyextextendedarticle_hnyextextendedarticleloadmore
                            }
                        }
                        hnyextextendedarticleloadlist {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Extended Article :: listLoad
                            description = List loaded via ajax
                            tt_content_defValues {
                                 CType = list
                                 list_type = hnyextextendedarticle_hnyextextendedarticleloadlist
                            }
                        }
                    }
                    show := addToList(hnyextextendedarticlelist,hnyextextendedarticleshow,hnyextextendedarticleloadmore,hnyextextendedarticleloadlist)
                }
            }'
        );
    }, $_EXTKEY
);