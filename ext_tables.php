<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticlelist',
            'hny_ext_extended_article :: List'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleshow',
            'hny_ext_extended_article : show'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleloadmore',
            'hny_ext_extended_article :: loadMore'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HNY.HnyExtExtendedArticle',
            'Hnyextextendedarticleloadlist',
            'hny_ext_extended_article : loadList'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hny_ext_extended_article', 'Configuration/TypoScript', 'hny_ext_extended_article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hnyextextendedarticle_domain_model_article', 'EXT:hny_ext_extended_article/Resources/Private/Language/locallang_csh_tx_hnyextextendedarticle_domain_model_article.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hnyextextendedarticle_domain_model_article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hny_ext_extended_article',
            'tx_hnyextextendedarticle_domain_model_article'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func_array(
    function ($extkey, &$tca) {
        $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extkey));
        $pluginName = strtolower('Hnyextextendedarticlelist');
        $pluginSignature = $extensionName . '_' . $pluginName;
        $tca['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
        $tca['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
            'FILE:EXT:' . $extkey . '/Configuration/FlexForms/Hnyextextendedarticlelist.xml');

        $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extkey));
        $pluginName = strtolower('Hnyextextendedarticleloadmore');
        $pluginSignature = $extensionName . '_' . $pluginName;
        $tca['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
        $tca['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
            'FILE:EXT:' . $extkey . '/Configuration/FlexForms/Hnyextextendedarticleloadmore.xml');

    }, [$_EXTKEY, &$TCA]
);