<?php
namespace HNY\HnyExtExtendedArticle\Domain\Repository;

/***
 *
 * This file is part of the "hny_ext_extended_article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for ExtendedArticles
 */
class ArticleRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HNY\\HnyExtExtendedArticle\\Domain\\Model\\Article';
        $sUserFuncPlugin = 'tx_hnyextextendedarticle';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
