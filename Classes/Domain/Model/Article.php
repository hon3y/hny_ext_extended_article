<?php
namespace HNY\HnyExtExtendedArticle\Domain\Model;

/***
 *
 * This file is part of the "hny_ext_extended_article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ExtendedArticle
 */
class Article extends \HIVE\HiveExtArticle\Domain\Model\Article
{
    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * string1
     *
     * @var string
     */
    protected $string1 = '';

    /**
     * string2
     *
     * @var string
     */
    protected $string2 = '';

    /**
     * string3
     *
     * @var string
     */
    protected $string3 = '';

    /**
     * string4
     *
     * @var string
     */
    protected $string4 = '';

    /**
     * text1
     *
     * @var string
     */
    protected $text1 = '';

    /**
     * text2
     *
     * @var string
     */
    protected $text2 = '';

    /**
     * text3
     *
     * @var string
     */
    protected $text3 = '';

    /**
     * dateTime1
     *
     * @var \DateTime
     */
    protected $dateTime1 = null;

    /**
     * dateTime2
     *
     * @var \DateTime
     */
    protected $dateTime2 = null;

    /**
     * dateTime3
     *
     * @var \DateTime
     */
    protected $dateTime3 = null;

    /**
     * dateTime4
     *
     * @var string
     */
    protected $dateTime4 = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @cascade remove
     */
    protected $image = null;

    /**
     * file
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @cascade remove
     */
    protected $file = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->image = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->file = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the string1
     *
     * @return string $string1
     */
    public function getString1()
    {
        return $this->string1;
    }

    /**
     * Sets the string1
     *
     * @param string $string1
     * @return void
     */
    public function setString1($string1)
    {
        $this->string1 = $string1;
    }

    /**
     * Returns the string2
     *
     * @return string $string2
     */
    public function getString2()
    {
        return $this->string2;
    }

    /**
     * Sets the string2
     *
     * @param string $string2
     * @return void
     */
    public function setString2($string2)
    {
        $this->string2 = $string2;
    }

    /**
     * Returns the string3
     *
     * @return string $string3
     */
    public function getString3()
    {
        return $this->string3;
    }

    /**
     * Sets the string3
     *
     * @param string $string3
     * @return void
     */
    public function setString3($string3)
    {
        $this->string3 = $string3;
    }

    /**
     * Returns the string4
     *
     * @return string $string4
     */
    public function getString4()
    {
        return $this->string4;
    }

    /**
     * Sets the string4
     *
     * @param string $string4
     * @return void
     */
    public function setString4($string4)
    {
        $this->string4 = $string4;
    }

    /**
     * Returns the text1
     *
     * @return string $text1
     */
    public function getText1()
    {
        return $this->text1;
    }

    /**
     * Sets the text1
     *
     * @param string $text1
     * @return void
     */
    public function setText1($text1)
    {
        $this->text1 = $text1;
    }

    /**
     * Returns the text2
     *
     * @return string $text2
     */
    public function getText2()
    {
        return $this->text2;
    }

    /**
     * Sets the text2
     *
     * @param string $text2
     * @return void
     */
    public function setText2($text2)
    {
        $this->text2 = $text2;
    }

    /**
     * Returns the text3
     *
     * @return string $text3
     */
    public function getText3()
    {
        return $this->text3;
    }

    /**
     * Sets the text3
     *
     * @param string $text3
     * @return void
     */
    public function setText3($text3)
    {
        $this->text3 = $text3;
    }

    /**
     * Returns the dateTime1
     *
     * @return \DateTime $dateTime1
     */
    public function getDateTime1()
    {
        return $this->dateTime1;
    }

    /**
     * Sets the dateTime1
     *
     * @param \DateTime $dateTime1
     * @return void
     */
    public function setDateTime1(\DateTime $dateTime1)
    {
        $this->dateTime1 = $dateTime1;
    }

    /**
     * Returns the dateTime2
     *
     * @return \DateTime $dateTime2
     */
    public function getDateTime2()
    {
        return $this->dateTime2;
    }

    /**
     * Sets the dateTime2
     *
     * @param \DateTime $dateTime2
     * @return void
     */
    public function setDateTime2(\DateTime $dateTime2)
    {
        $this->dateTime2 = $dateTime2;
    }

    /**
     * Returns the dateTime3
     *
     * @return \DateTime $dateTime3
     */
    public function getDateTime3()
    {
        return $this->dateTime3;
    }

    /**
     * Sets the dateTime3
     *
     * @param \DateTime $dateTime3
     * @return void
     */
    public function setDateTime3(\DateTime $dateTime3)
    {
        $this->dateTime3 = $dateTime3;
    }

    /**
     * Returns the dateTime4
     *
     * @return string $dateTime4
     */
    public function getDateTime4()
    {
        return $this->dateTime4;
    }

    /**
     * Sets the dateTime4
     *
     * @param string $dateTime4
     * @return void
     */
    public function setDateTime4($dateTime4)
    {
        $this->dateTime4 = $dateTime4;
    }

    /**
     * Adds a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image->attach($image);
    }

    /**
     * Removes a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
     * @return void
     */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove)
    {
        $this->image->detach($imageToRemove);
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image)
    {
        $this->image = $image;
    }

    /**
     * Adds a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     * @return void
     */
    public function addFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $this->file->attach($file);
    }

    /**
     * Removes a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove The FileReference to be removed
     * @return void
     */
    public function removeFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove)
    {
        $this->file->detach($fileToRemove);
    }

    /**
     * Returns the file
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $file
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets the file
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $file
     * @return void
     */
    public function setFile(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $file)
    {
        $this->file = $file;
    }
}
