<?php
namespace HNY\HnyExtExtendedArticle\Controller;

/***
 *
 * This file is part of the "hny_ext_extended_article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ArticleController
 */
class ArticleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * articleRepository
     *
     * @var \HNY\HnyExtExtendedArticle\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     *
     * @param HNY\HnyExtArticle\Domain\Model\Article
     * @return void
     */
    public function listAction()
    {
        $extendedArticles = $this->extendedArticleRepository->findAll();
        $this->view->assign('extendedArticles', $extendedArticles);
    }

    /**
     * action show
     *
     * @param HNY\HnyExtArticle\Domain\Model\Article
     * @return void
     */
    public function showAction(\HNY\HnyExtExtendedArticle\Domain\Model\Article $article)
    {
        $this->view->assign('extendedArticle', $extendedArticle);
    }

    /**
     * action loadMore
     *
     * @param HNY\HnyExtArticle\Domain\Model\Article
     * @return void
     */
    public function loadMoreAction()
    {

    }

    /**
     * action loadList
     *
     * @param HNY\HnyExtArticle\Domain\Model\Article
     * @return void
     */
    public function loadListAction()
    {

    }
}
