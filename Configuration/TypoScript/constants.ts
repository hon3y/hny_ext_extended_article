
plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist {
    view {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hny_ext_extended_article/Resources/Private/Templates/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hny_ext_extended_article/Resources/Private/Partials/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hny_ext_extended_article/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow {
    view {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hny_ext_extended_article/Resources/Private/Templates/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hny_ext_extended_article/Resources/Private/Partials/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hny_ext_extended_article/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore {
    view {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hny_ext_extended_article/Resources/Private/Templates/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hny_ext_extended_article/Resources/Private/Partials/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hny_ext_extended_article/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist {
    view {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hny_ext_extended_article/Resources/Private/Templates/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hny_ext_extended_article/Resources/Private/Partials/
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hny_ext_extended_article/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hnyextextendedarticle {
        model {
            HNY\HnyExtExtendedArticle\Domain\Model\Article {
                persistence {
                    storagePid =
                }
            }
        }
    }
}