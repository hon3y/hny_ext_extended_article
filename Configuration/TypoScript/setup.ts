
plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist {
    view {
        templateRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist.view.templateRootPath}
        partialRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow {
    view {
        templateRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow.view.templateRootPath}
        partialRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow.view.partialRootPath}
        layoutRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore {
    view {
        templateRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore.view.templateRootPath}
        partialRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore.view.partialRootPath}
        layoutRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist {
    view {
        templateRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist.view.templateRootPath}
        partialRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hny_ext_extended_article/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hnyextextendedarticle._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hny-ext-extended-article table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hny-ext-extended-article table th {
        font-weight:bold;
    }

    .tx-hny-ext-extended-article table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hnyextextendedarticle {
        model {
            HNY\HnyExtExtendedArticle\Domain\Model\Article {
                persistence {
                    storagePid = {$plugin.tx_hnyextextendedarticle.model.HNY\HnyExtExtendedArticle\Domain\Model\Article.persistence.storagePid}
                }
            }
        }
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticlelist {
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle.model.HNY\HnyExtExtendedArticle\Domain\Model\Article.persistence.storagePid}
        #recursive = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleshow {
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle.model.HNY\HnyExtExtendedArticle\Domain\Model\Article.persistence.storagePid}
        #recursive = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadmore {
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle.model.HNY\HnyExtExtendedArticle\Domain\Model\Article.persistence.storagePid}
        #recursive = 1
    }
}

plugin.tx_hnyextextendedarticle_hnyextextendedarticleloadlist {
    persistence {
        storagePid = {$plugin.tx_hnyextextendedarticle.model.HNY\HnyExtExtendedArticle\Domain\Model\Article.persistence.storagePid}
        #recursive = 1
    }
}