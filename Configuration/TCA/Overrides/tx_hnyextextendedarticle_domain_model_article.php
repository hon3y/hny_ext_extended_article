<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

$sExtendedModel = 'tx_hiveextarticle_domain_model_article';
$GLOBALS['TCA'][$sModel]['columns']['title'] = $GLOBALS['TCA'][$sExtendedModel]['columns']['title'];
$GLOBALS['TCA'][$sModel]['columns']['text'] = $GLOBALS['TCA'][$sExtendedModel]['columns']['text'];
$GLOBALS['TCA'][$sModel]['columns']['meta_data'] = $GLOBALS['TCA'][$sExtendedModel]['columns']['meta_data'];
$GLOBALS['TCA'][$sModel]['columns']['taxonomy'] = $GLOBALS['TCA'][$sExtendedModel]['columns']['taxonomy'];
$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = 'title,text,meta_data,taxonomy' . ',' . $GLOBALS['TCA'][$sModel]['types']['1']['showitem'];

